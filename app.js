const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session'); // session Middleware
const MongoDBStore = require('connect-mongodb-session')(session); 

const errorController = require('./controllers/error');
const User = require('./models/user');

/** MONGO DB token/credentials */
const MONGODB_URI = 'mongodb+srv://johnfuentesguzman:kurtco235@cluster0.4dsni.mongodb.net/shopStore'

const app = express();

// save session by middleware in Mongo DB  
const store = new MongoDBStore({
  uri: MONGODB_URI,
  collection: 'sessions' // mongoDB collection name
});


app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');

// app.use is an instance that set this data in every request for any particular routes (/routes/)
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


 // setting session middleware globally and beeing saved in MongoDB (store:store)
app.use(
  session({
    secret: 'my secret',
    resave: false,
    saveUninitialized: false,
    store: store
  })
);

app.use((req, res, next) => {
  User.findById('6000ced55674a5e3ebe70fb7')
    .then(user => {
      req.user = user;
      next();
    })
    .catch(err => console.log(err));
});

app.use('/admin', adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);

app.use(errorController.get404);

mongoose
  .connect(
    MONGODB_URI
  )
  .then(result => {
    User.findOne().then(user => {
      if (!user) {
        const user = new User({
          name: 'John Fuentes',
          email: 'kurtco.5@gmail.com',
          cart: {
            items: []
          }
        });
        user.save();
      }
    });
    app.listen(3000);
  })
  .catch(err => {
    console.log(err);
  });
