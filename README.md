# README #
Handling session and cookies from mongo

### What will I learn? ###

* Configuring Cookies

* Creating a session Middleware:  npm i --save express-session ... https://github.com/expressjs/session

* Using a mongoDB store sessions:   https://www.npmjs.com/package/connect-mongodb-session
