const User = require('../models/user');


exports.getLogin = (req, res, next) => {
  res.render('auth/login', {
    path: '/login',
    pageTitle: 'Login',
    isAuthenticated: false
  });
};

exports.postLogin = (req, res, next) => {
  // HttpOnly in cookie means .. the cookie can not be access/modified to javascript/client 
  // res.setHeader('Set-Cookie', 'loggedIn=true ; HttpOnly'); // setting cookie to be present in all project routes/controllers/models
  


  /** using created session midleware: 
   * you are able to set the data encrypted in the browser as cookies to be saved on the db,
   *  just adding a new atrribute to the session object */

  User.findById('6000ced55674a5e3ebe70fb7')
  .then(user => {
    req.session.isLoggedIn = true;
    req.session.user = user;
    req.session.save(err => {
      console.log(err);
      res.redirect('/');
    });
  })
  .catch(err => console.log(err));
};


exports.postLogout = (req, res, next) => {
  req.session.destroy(err => {
    console.log(err);
    res.redirect('/');
  });
};

